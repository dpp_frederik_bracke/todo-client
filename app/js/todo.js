function init() {
    var ROOT = 'https://buoyant-sum-777.appspot.com/_ah/api';
    gapi.client.load('todo', 'v1', function() {
        gapi.client.todo.todo.list().execute(function(resp){
            var itemList = resp.items;
            itemList.sort(sortByNumber);
            $.each(resp.items, function(idx, object) {
                console.log(object);
                $('#todoList').append('<li>' + object.name + '</li>');
            });
            console.log(resp);
        });
    }, ROOT);
}

function sortByNumber(a, b) {
    return ((a.sort < b.sort) ? -1 : ((a.sort > b.sort) ? 1 : 0));
}

function saveTodo() {
    var todo = $('#todoForm').serializeObject();
    todo.done =false;
    gapi.client.todo.todo.save(todo).execute(function(resp) {
        if(resp.id) {
            var message = 'Successfully saved todo ' + resp.name + ' with id ' + resp.id;
            console.log(message);
        }
    });
}